package ph.groundgurus.day3;

public class AnimalMain {
    public static void main(String[] args) {
        Animal dog = new Dog(); // valid
        dog.speak();

        Animal cat = new Cat(); // valid
        cat.speak();

        // two types of polymorphism in programming
        // compile-time
        // runtime (Java)
    }
}
