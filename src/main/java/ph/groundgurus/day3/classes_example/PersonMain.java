package ph.groundgurus.day3.classes_example;

public class PersonMain {
    public static void main(String[] args) {
        var joe = new Person("Joe", 30, "", "", 0.0);
        System.out.println(joe.getName());
        System.out.println(joe.getAge());
    }
}
