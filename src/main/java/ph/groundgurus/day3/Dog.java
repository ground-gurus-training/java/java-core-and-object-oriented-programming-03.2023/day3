package ph.groundgurus.day3;

public class Dog extends Animal {
    @Override
    public void speak() {
        System.out.println("bark!");
    }
}
